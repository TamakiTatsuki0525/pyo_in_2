using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ブロックの動作等を管理するスクリプト
/// </summary>
public class BlockSystem : MonoBehaviour
{
    [System.NonSerialized] public bool IsStop = false;//止まるかどうか
    [System.NonSerialized] public int BlockColor;//ブロックの色のナンバー
    public int BlockState;
    public int Xpos;//配列に置けるXの場所
    public int Ypos;//配列におけるYの場所

    //private
    GameObject GameSystemObject;
    ConstantStorage ConstantStorageCS;
    GameSystemManager GameSystemManagerCS;
    SpriteRenderer SpriteRenderIP;
    Animator _Anim;
    AnimManager AnimManagerCS;

    bool IsStore;
    int OtherBlockState;
    public int BlocKPosition;
    int _counter;
    int _StoreState;

    // Start is called before the first frame update
    void Start()
    {
        //システム関係取得
        GameSystemObject = GameObject.FindGameObjectWithTag("GameSystem");
        ConstantStorageCS = GameSystemObject.GetComponent<ConstantStorage>();
        GameSystemManagerCS = GameSystemObject.GetComponent<GameSystemManager>();

        BlockInitFunc();//初期化

        //メーション関係取得
        _Anim = GetComponent<Animator>();
        AnimManagerCS = GetComponent<AnimManager>();

        AnimManagerCS.AnimInitManager(BlockColor);
    }

    // Update is called once per frame
    void Update()
    {
        JugdeErase();
    }

    public void PositionChanger(int InsDirect)
    {
        //下
        if (InsDirect == ConstantStorageCS.Down)
        {
            this.gameObject.transform.position += new Vector3(0, -ConstantStorageCS.MasSize, 0);//描画座標移動
            Ypos--;
        }

        //左
        else if (InsDirect == ConstantStorageCS.Left)
        {
            this.gameObject.transform.position += new Vector3(-ConstantStorageCS.MasSize, 0, 0);//描画座標移動
            Xpos--;
        }
        
        //右
        else if (InsDirect == ConstantStorageCS.Right)
        {
            this.gameObject.transform.position += new Vector3(ConstantStorageCS.MasSize, 0, 0);//描画座標移動
            Xpos++;
        }

        //右上
        else if (InsDirect == ConstantStorageCS.UpRight)
        {
            this.gameObject.transform.position += 
                new Vector3(ConstantStorageCS.MasSize, ConstantStorageCS.MasSize, 0);//描画座標移動
            Xpos++;
            Ypos++;
        }

        //左上
        else if (InsDirect == ConstantStorageCS.UpLeft)
        {
            this.gameObject.transform.position +=
                new Vector3(-ConstantStorageCS.MasSize, ConstantStorageCS.MasSize, 0);//描画座標移動
            Xpos--;
            Ypos++;
        }

        //右下
        else if (InsDirect == ConstantStorageCS.DownRight)
        {
            this.gameObject.transform.position +=
               new Vector3(ConstantStorageCS.MasSize, -ConstantStorageCS.MasSize, 0);//描画座標移動
            Xpos++;
            Ypos--;
        }

        //左下
        else if (InsDirect == ConstantStorageCS.DownLeft)
        {
            this.gameObject.transform.position +=
               new Vector3(-ConstantStorageCS.MasSize, -ConstantStorageCS.MasSize, 0);//描画座標移動
            Xpos--;
            Ypos--;
        }
    }

    
    //初期化
    void BlockInitFunc()
    {
        //画像関係取得（本来はアニメーション）
        SpriteRenderIP = GetComponent<SpriteRenderer>();

        //一旦おいとく
        BlockColor = Random.Range(1, 6 + 1);

        Xpos = 3;

        if (this.transform.position.y == ConstantStorageCS.UpInstancePos)
        { 
            GameSystemManagerCS.BlockList[Xpos, 8] = BlockColor;
            Ypos = 8;
            BlockState = ConstantStorageCS.Over;
            BlocKPosition = ConstantStorageCS.OutSide;
        }

        if (this.transform.position.y == ConstantStorageCS.UnderInstancePos)
        {
            //デバッグがやりやすいように置いておく
            BlockColor = ConstantStorageCS.Blue;

            GameSystemManagerCS.BlockList[Xpos, 7] = BlockColor;
            Ypos = 7;
            BlockState = ConstantStorageCS.Under;
            BlocKPosition = ConstantStorageCS.Center;
        }
    }
    
    
    //ぷよを消すかどうかの判断
    void JugdeErase()
    {
        //消せるかどうか判断してほしいとメインシステムに言われた時、
        if (GameSystemManagerCS.IsSearchDestory)
        {
            for (int x = 0;x < 6;x++)
            {
                for (int y = 0;y < 9;y++)
                {
                    //消す配列に自分が合ったら
                    if (GameSystemManagerCS.BlockEraseList[x,y] && Xpos == x && Ypos == y)
                    {
                        GameSystemManagerCS.BlockList[x, y] = ConstantStorageCS.BlockNone;
                        GameSystemManagerCS.BlockEraseList[x, y] = false;
                        AnimManagerCS.IsDestory = true;
                    }
                }
            }
        }
    }
}
