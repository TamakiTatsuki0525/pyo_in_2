using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//アニメーションを管理するスクリプト
public class AnimManager : MonoBehaviour
{
    //public
    [System.NonSerialized]public bool IsDestory = false;//破壊してもいいか（ブロックスクリプトから代入される）

    //private
    Animator _anim;
    StateManager StateManagerCS;
    GameObject GameSystemObject;
    GameSystemManager GameSystemManagerCS;

    private void Start()
    {
        //システム関係取得
        GameSystemObject = GameObject.FindGameObjectWithTag("GameSystem");
        GameSystemManagerCS = GameSystemObject.GetComponent<GameSystemManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDestory)
        {
            _anim.SetBool("IsDestory", true);
        }

        if (StateManagerCS.AnimFinish)
        {
            GameSystemManagerCS.Score += 10;
            Destroy(this.gameObject);
        }
    }

    //アニメーション初期化
    public void AnimInitManager(int InsColorNum)
    {
        _anim = GetComponent<Animator>();
        StateManagerCS = _anim.GetBehaviour<StateManager>();
        _anim.SetInteger("AnimNunber",InsColorNum - 1);
    }

}
