using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ブロックを生成するスクリプト、基本的に生成のみを行う
/// </summary>
public class BlockInstanced : MonoBehaviour
{
    //public
    public GameObject BlockObject;

    //private
    GameObject GameSystemObject;
    ConstantStorage ConstantStorageCS;
    GameSystemManager GameSystemManagerCS;

    // Start is called before the first frame update
    void Start()
    {
        //システム関係取得
        GameSystemObject = GameObject.FindGameObjectWithTag("GameSystem");
        ConstantStorageCS = GameSystemObject.GetComponent<ConstantStorage>();
        GameSystemManagerCS = GameSystemObject.GetComponent<GameSystemManager>();
    }

    // Update is called once per frame
    void Update()
    {
        BlockInstancedFun();
    }

    void BlockInstancedFun()
    {
        //生成していい場合
        if (GameSystemManagerCS.IsInstantiate)
        {
            //生成の処理
            //ぷよ１個め
            Vector3 _VecUP = new Vector3(ConstantStorageCS.InstancePosX,ConstantStorageCS.UpInstancePos,0);
            Instantiate(BlockObject, _VecUP, Quaternion.identity);

            //ぷよ２個め
            Vector3 _VecUnder = new Vector3(ConstantStorageCS.InstancePosX, ConstantStorageCS.UnderInstancePos, 0);
            Instantiate(BlockObject, _VecUnder, Quaternion.identity);

            GameSystemManagerCS.IsInstantiate = false;
        }
    }
}
