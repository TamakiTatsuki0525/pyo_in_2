using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 定数置き場、システムに貼り付けてゲットする形
/// </summary>
public class ConstantStorage : MonoBehaviour
{
    //色のナンバー
    public readonly int Blue = 1;
    public readonly int gray = 2;
    public readonly int green = 3;
    public readonly int purple = 4;
    public readonly int red = 5;
    public readonly int yellow = 6;
    public readonly int TmpDeleteColor = 7;

    public readonly int BlockNone = 64;

    //1マスのサイズ
    public readonly float MasSize = 1.1f;
    public readonly float HalfMasSize = 0.55f;

    //生成の位置
    public readonly float UpInstancePos = 4.675f;
    public readonly float UnderInstancePos = 3.575f;
    public readonly float InstancePosX = 0.55f;

    //時間関係
    public readonly float FallProcessTime = 1.0f;
    public readonly float jugdeProcessTime = 0.5f;

    //方向
    public readonly int Up = 0;
    public readonly int Down = 1;
    public readonly int Left = 2;
    public readonly int Right = 3;
    public readonly int UpRight = 4;
    public readonly int UpLeft = 5;
    public readonly int DownRight = 6;
    public readonly int DownLeft = 7;
    public readonly int DirectNone = 64;

    //ぷよの状態
    public readonly int Over = 0;
    public readonly int Under = 1;
    public readonly int Center = 4;
    public readonly int OutSide = 5;

    //システムの段階用の変数
    public readonly int PhaseNone = 0;
    public readonly int EraseBlockPhase = 1;
    public readonly int AfterErasePhase = 2;
    public readonly int InstantatePhase = 3;
    public readonly int MovePhase = 4;
    public readonly int EraseSearthPhase = 5;


}
