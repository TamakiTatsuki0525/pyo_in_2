using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// AI用のスクリプト。
/// FlagがTrueになった場合、True
/// </summary>
public class AIPlayerCS : MonoBehaviour
{
    //public
    public bool IsAIPlayerActive = false;

    //以下どの動作を何回行うかの数字
    public int RightRotateQua = 0;
    public int LeftRotateQua = 0;
    public int RightMoveQua= 0;
    public int LeftMoveQua = 0;

    //private
    GameObject GameSystemObject;
    ConstantStorage ConstantStorageCS;
    GameSystemManager GameSystemManagerCS;

    bool IsSearthFinish = false;
    bool OverExistPuyo = false;
    bool[,] RevivalPuyoList = new bool[6,9];
    int[,] PuyoAlgoList = new int[6,9];
    int[,] CopyPuyoAlgoList = new int[6,9];
    int[,] MaybeEraseList = new int[6,9];

    struct OperatePuyoDateStruct
    {
        int State;
        int Position;
        int _Color;

        public void Seter(int InsState,int InsPosition,int InsColor)
        {
            State = InsState;
            Position = InsPosition;
            _Color = InsColor;
        }

        public int StateGeter()
        {
            return State;
        }

        public int PositionGeter()
        {
            return Position;
        }

        public int ColorGeter()
        {
            return _Color;
        }
    }

    //取得時は０が下、１が上
    OperatePuyoDateStruct[] OperatePuyoDates = new OperatePuyoDateStruct[2];

    // Start is called before the first frame update
    void Start()
    {
        //システム関係取得
        GameSystemObject = GameObject.FindGameObjectWithTag("GameSystem");
        ConstantStorageCS = GameSystemObject.GetComponent<ConstantStorage>();
        GameSystemManagerCS = GameSystemObject.GetComponent<GameSystemManager>();

    }

    //ぷよをどこに置くかを決める。
    public void PuyoAlgo()
    {
        //変数初期化
        RightMoveQua = 0;
        RightRotateQua = 0;
        LeftMoveQua = 0;
        LeftRotateQua = 0;


        GetPuyoDate();//ぷよのデータを取得
        SerthPuyoErase();//２個もしくは３個続いてる場所を探す

        //まず初めに左上から3個続いてる場所の１番上を探す
        for (int x = 0; x < 6;x++)
        {
            //調べ終わってる
            if (IsSearthFinish)
            {
                break;
            }
            for (int y = 8;y >= 0;y--)
            {
                //調べ終わってる
                if (IsSearthFinish)
                {
                    break;
                }
                //３個以上消すかもの配列がある
                if (MaybeEraseList[x,y] != ConstantStorageCS.BlockNone && MaybeEraseList[x, y] > 0) 
                {
                    //下、上の順で調べる
                    for (int i = 0; i < 2 ; i++)
                    {
                        //調べ終わってる
                        if (IsSearthFinish)
                        {
                            break;
                        }

                        //色あった
                        if (MaybeEraseList[x,y] == OperatePuyoDates[i].ColorGeter())
                        {
                            OverExistPuyo = false;
                            //その上にブロックが無いか調べる
                            for (int k = 8; k > y; k--)
                            {
                                if (PuyoAlgoList[x,k] != ConstantStorageCS.BlockNone)
                                {
                                    OverExistPuyo = true;
                                }
                            }
                            if (!OverExistPuyo)
                            {
                                //下
                                if (OperatePuyoDates[i].StateGeter() == ConstantStorageCS.Under)
                                {
                                    int _num = x - 3;
                                    if (_num > 0)
                                    {
                                        RightMoveQua += _num;
                                    }
                                    else if (_num < 0)
                                    {
                                        LeftMoveQua -= _num;
                                    }
                                }
                                //上
                                else
                                {
                                    RightRotateQua += 2;
                                    int _num = x - 3;
                                    if (_num > 0)
                                    {
                                        RightMoveQua += _num;
                                    }
                                    else if (_num < 0)
                                    {
                                        LeftMoveQua -= _num;
                                    }
                                }
                                Debug.Log("3個");
                                IsSearthFinish = true;
                            }                           
                        }
                    }
                }
            }
        }

        //2個め
        if (!IsSearthFinish)
        {
            for (int x = 0; x < 6; x++)
            {
                //調べ終わってる
                if (IsSearthFinish)
                {
                    break;
                }
                for (int y = 8; y >= 0; y--)
                {
                    //調べ終わってる
                    if (IsSearthFinish)
                    {
                        break;
                    }
                    //３個以上消すかもの配列がある
                    if (MaybeEraseList[x, y] != ConstantStorageCS.BlockNone && MaybeEraseList[x, y] < 0)
                    {
                        //下、上の順で調べる
                        for (int i = 0; i < 2; i++)
                        {
                            //調べ終わってる
                            if (IsSearthFinish)
                            {
                                break;
                            }

                            //色あった
                            if ((-MaybeEraseList[x, y])== OperatePuyoDates[i].ColorGeter())
                            {
                                OverExistPuyo = false;
                                //その上にブロックが無いか調べる
                                for (int k = 8; k > y; k--)
                                {
                                    if (PuyoAlgoList[x, k] != ConstantStorageCS.BlockNone)
                                    {
                                        OverExistPuyo = true;
                                    }
                                }
                                if (!OverExistPuyo)
                                {
                                    //下
                                    if (OperatePuyoDates[i].StateGeter() == ConstantStorageCS.Under)
                                    {
                                        int _num = x - 3;
                                        if (_num > 0)
                                        {
                                            RightMoveQua += _num;
                                        }
                                        else if (_num < 0)
                                        {
                                            LeftMoveQua -= _num;
                                        }
                                    }
                                    //上
                                    else
                                    {
                                        RightRotateQua += 2;
                                        int _num = x - 3;
                                        if (_num > 0)
                                        {
                                            RightMoveQua += _num;
                                        }
                                        else if (_num < 0)
                                        {
                                            LeftMoveQua -= _num;
                                        }
                                    }
                                    Debug.Log("2個");
                                    IsSearthFinish = true;
                                }                              
                            }
                        }
                    }
                }
            }
            //もしそれでも無かったら
            for (int y = 0; y < 9; y++)
            {
                //調べ終わってる
                if (IsSearthFinish)
                {
                    break;
                }
                for (int x = 0; x < 6; x++)
                {
                    //調べ終わってる
                    if (IsSearthFinish)
                    {
                        break;
                    }
                    if (PuyoAlgoList[x,y] == ConstantStorageCS.BlockNone)
                    {
                        int _num = x - 3;
                        //右
                        if (_num > 0)
                        {
                            RightMoveQua += _num;
                        }
                        else if (_num < 0)
                        {
                            LeftMoveQua -= _num;
                        }

                        IsSearthFinish = true;
                        break;
                    }
                }
            }
        }
        Debug.Log(RightMoveQua + "  右移動");
        Debug.Log(LeftMoveQua + "  左移動");
        Debug.Log(RightRotateQua + "  右かいてん");
        Debug.Log(LeftRotateQua + "  左かいてん");


        IsSearthFinish = false;
    }

    //ぷよのデータを取得
    void GetPuyoDate()
    {
        for (int X = 0; X < 6; X++)
        {
            for (int Y = 0; Y < 9; Y++)
            {
                PuyoAlgoList[X, Y] = ConstantStorageCS.BlockNone;
                if (GameSystemManagerCS.BlockList[X, Y] != ConstantStorageCS.BlockNone)
                {
                    PuyoAlgoList[X, Y] = GameSystemManagerCS.BlockList[X, Y];
                    if (X == 3)
                    {
                        //下
                        if (Y == 7)
                        {
                            //Debug.Log("0個目");
                            OperatePuyoDates[0].Seter(ConstantStorageCS.Under, ConstantStorageCS.Center, GameSystemManagerCS.BlockList[X, Y]);
                        }
                        //上
                        else
                        {
                            //Debug.Log("1個目");
                            OperatePuyoDates[1].Seter(ConstantStorageCS.Over, ConstantStorageCS.OutSide, GameSystemManagerCS.BlockList[X, Y]);
                        }
                    }
                }
            }
        }

        //for (int i = 0;i < 2; i++)
        //{
        //    Debug.Log("Num  " + i + "  State  " + OperatePuyoDates[i].StateGeter() +
        //        "   Position  " + OperatePuyoDates[i].PositionGeter() + "  Color  " + OperatePuyoDates[i].ColorGeter());
        //}

    }

    void SerthPuyoErase()
    {
        //再起処理配列の初期化
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                RevivalPuyoList[x, y] = false;
                MaybeEraseList[x, y] = ConstantStorageCS.BlockNone;
            }
        }

        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                if ((PuyoAlgoList[x, y] != ConstantStorageCS.BlockNone) && (!RevivalPuyoList[x, y]))
                {
                    CopyPuyoListFunc();//配列のこぴーを作成
                    RevivalPuyoList[x, y] = true;//調べた事がある判定にする
                    CountPuyo(x, y, 0, PuyoAlgoList[x, y]);//周りを調べる
                }
            }
        }


        for (int X = 0; X < 6; X++)
        {
            for (int Y = 0; Y < 9; Y++)
            {
                if (MaybeEraseList[X, Y] != ConstantStorageCS.BlockNone)
                {
                    Debug.Log("消すかもしれんところ　" + X + "  " + Y + "  " + MaybeEraseList[X, Y]);
                }
            }
        }

    }

    void CopyPuyoListFunc()
    {
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                CopyPuyoAlgoList[x, y] = PuyoAlgoList[x, y];
            }
        }

    }

    void CountPuyo(int _x, int _y, int StockNum, int InsBlockColor)
    {
        //下方向に同じ色のぷよがある場合(調べる中心の座標が１以上かつ下のぷよが同じ色かつその下のぷよを一回も調べたことが無い)
        if (_y >= 1 && PuyoAlgoList[_x, _y - 1] == InsBlockColor && !RevivalPuyoList[_x, _y - 1])
        {
            StockNum++;//連続する個数を足す
            RevivalPuyoList[_x, _y - 1] = true;//一回調べた状態にする。
            CopyPuyoAlgoList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyPuyoAlgoList[_x, _y - 1] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CountPuyo(_x, _y - 1, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の上方向
        if (_y <= 7 && PuyoAlgoList[_x, _y + 1] == InsBlockColor && !RevivalPuyoList[_x, _y + 1])
        {
            StockNum++;//連続する個数を足す
            RevivalPuyoList[_x, _y + 1] = true;//一回調べた状態にする。
            CopyPuyoAlgoList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyPuyoAlgoList[_x, _y + 1] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CountPuyo(_x, _y + 1, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の左方向
        if (_x >= 1 && PuyoAlgoList[_x - 1, _y] == InsBlockColor && !RevivalPuyoList[_x - 1, _y])
        {
            StockNum++;//連続する個数を足す
            RevivalPuyoList[_x - 1, _y] = true;//一回調べた状態にする。
            CopyPuyoAlgoList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyPuyoAlgoList[_x - 1, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CountPuyo(_x - 1, _y, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の右方向
        if (_x <= 4 && PuyoAlgoList[_x + 1, _y] == InsBlockColor && !RevivalPuyoList[_x + 1, _y])
        {
            StockNum++;//連続する個数を足す
            RevivalPuyoList[_x + 1, _y] = true;//一回調べた状態にする。
            CopyPuyoAlgoList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyPuyoAlgoList[_x + 1, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CountPuyo(_x + 1, _y, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //三個続き
        if (StockNum == 2)
        {
            for (int x = 0; x < 6; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    if (CopyPuyoAlgoList[x, y] == ConstantStorageCS.TmpDeleteColor && y != 7 && y != 8)
                    {
                        MaybeEraseList[x, y] = InsBlockColor;
                        Debug.Log("３こ続き" + InsBlockColor);
                    }
                }
            }
        }

        //２個続き
        else if (StockNum == 1)
        {
            for (int x = 0; x < 6; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    if (CopyPuyoAlgoList[x, y] == ConstantStorageCS.TmpDeleteColor && y != 7 && y != 8)
                    {
                        MaybeEraseList[x, y] = -InsBlockColor;
                        Debug.Log("2こ続き" + InsBlockColor);
                    }
                }
            }
        }
    }

}
