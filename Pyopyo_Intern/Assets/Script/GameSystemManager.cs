using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //UI機能を扱うときに追記する
/// <summary>
/// ゲームのシステムを管理するスクリプト
/// </summary>
public class GameSystemManager : MonoBehaviour
{
    //public 
    public Text ScoreText;
    public AIPlayerCS AIPlayer;

    [System.NonSerialized] public bool IsInstantiate = true;//ぷよを生成させる
    [System.NonSerialized] public bool IsFall = false;//ぷよに落とす処理をさせる
    [System.NonSerialized] public bool IsSearchDestory = false;//ぷよが消せるかどうか調べさせる
    [System.NonSerialized] public bool IsAfterInsert = false;//ぷよを消す段階が終わった後、ぷよを下に敷き詰める動作をさせる。

    [System.NonSerialized] public int[,] BlockList = new int[6,9];//ぷよを管理するリスト
    [System.NonSerialized] public bool[,] BlockEraseList = new bool[6,9];//消すぷよを管理するリスト

    [System.NonSerialized] public float ManagerTime = 0.0f;//時間。基本的に１秒経ったら動作が行われる。
    [System.NonSerialized] public bool IsCenterRotate = false;
    [System.NonSerialized] public int Score = 0;
    
    //private
    GameObject GameSystemObject;
    ConstantStorage ConstantStorageCS;

    bool IsGotoNextPhase = false;
    bool IsAIJudge = false;
    int SystemPhase = 0;//ぷよが着地した後の段階を管理する変数
    int LandingCounter;//着地下したらこれに数が足され、２になったらぷよが着地下判定になる。
    int[,] CopyBlockList = new int[6, 9];//ぷよを管理するリストの複製
    bool[,] RevivalBlockList = new bool[6, 9];//再起処理用の配列
    bool[,] AfterObjectNone = new bool[6, 9];
    bool[] IsDirectCountStart = new bool[4];
    bool[] IsDirectCountStop = new bool[4];

    private void Awake()
    {
        Application.targetFrameRate = 30;
    }

    // Start is called before the first frame update
    void Start()
    {
        //システム関係取得
        GameSystemObject = GameObject.FindGameObjectWithTag("GameSystem");
        ConstantStorageCS = GameSystemObject.GetComponent<ConstantStorage>();
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                BlockList[x, y] = ConstantStorageCS.BlockNone;//ブロックの配列初期化
            }
        }
        SystemPhase = ConstantStorageCS.MovePhase;
    }

    // Update is called once per frame
    void Update()
    {
        TimeManagerFun();//時間管理関数
        BlockMoveManager();
        BlockManagerfunc();//ブロックを管理する関数
        ScoreText.text = "Score:" + Score.ToString();

    }

    //ゲームオーバーも作成
    //bool GameOver()
    //{
    //    for (int _x = 0;_x < 6;_x++)
    //    {
    //        if ()
    //        {

    //        }
    //    }
    //    return false;
    //}

    //ブロックの動きを管理する関数
    void BlockMoveManager()
    {
        if (AIPlayer.IsAIPlayerActive)
        {
            if (IsAIJudge)
            {
                //動かす手順を決める
                AIPlayer.PuyoAlgo();

                //ココからは決まった手順通りに動かせる
                //左移動
                if (AIPlayer.LeftMoveQua > 0)
                {
                    for (int a = 0; a <AIPlayer.LeftMoveQua;a++)
                    {
                        BlockVertivalMove(ConstantStorageCS.Left);
                    }
                }

                //右移動
                if (AIPlayer.RightMoveQua > 0)
                {
                    for (int a = 0; a < AIPlayer.RightMoveQua; a++)
                    {
                        BlockVertivalMove(ConstantStorageCS.Right);
                    }
                }

                //左回転
                if (AIPlayer.LeftRotateQua > 0)
                {
                    for (int a = 0; a < AIPlayer.LeftRotateQua; a++)
                    {
                        BlockTurn(ConstantStorageCS.Left);
                    }
                }

                //右回転
                if (AIPlayer.RightRotateQua > 0)
                {
                    for (int a = 0; a < AIPlayer.RightRotateQua; a++)
                    {
                        BlockTurn(ConstantStorageCS.Right);
                    }
                }

                //最後に落下
                GotoMostUnder();

                IsAIJudge = false;
            }
        }

        else
        {
            //落下状態ではないとき
            if (!IsFall)
            {
                //左移動
                if (Input.GetKeyDown(KeyCode.A) && CanMoveSer(ConstantStorageCS.Left))
                {
                    BlockVertivalMove(ConstantStorageCS.Left);
                }

                //右移動
                else if (Input.GetKeyDown(KeyCode.D) && CanMoveSer(ConstantStorageCS.Right))
                {
                    BlockVertivalMove(ConstantStorageCS.Right);
                }


                //右回転
                else if (Input.GetKeyDown(KeyCode.L) && CanTurnSer(ConstantStorageCS.Right))
                {
                    BlockTurn(ConstantStorageCS.Right);
                }

                //左回転
                else if (Input.GetKeyDown(KeyCode.J) && CanTurnSer(ConstantStorageCS.Left))
                {
                    BlockTurn(ConstantStorageCS.Left);
                }

                //一番下までぷよが行く
                else if (Input.GetKeyDown(KeyCode.Space))
                {
                    GotoMostUnder();
                }
            }
            //落下状態
            else
            {
                BlockFall();//落下してよし
            }

        }
    }

    void BlockFall()
    {
        //ぷよをゲット
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            //操作中の場合
            if (!BlockSystemCS.IsStop)
            {
                //下へ移動
                BlockSystemCS.PositionChanger(ConstantStorageCS.Down);
            }
        }
        StoreList(ConstantStorageCS.Down);//配列を格納
        IsBlockStop();//着地下かを調べる
    }

    //着地したかを調べる
    void IsBlockStop()
    {
        //ぷよをゲット
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            //操作中の場合
            if (!BlockSystemCS.IsStop)
            {
                //中心よりうえにある
                if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                {
                    //
                    if (BlockSystemCS.Ypos <= 1 || BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos - 2] != ConstantStorageCS.BlockNone)
                    {
                        BlockSystemCS.IsStop = true;
                        LandingCounter++;
                    }
                }
                //それ以外
                else
                {
                    //最下層もしくは１こ下にブロックがある場合
                    if (BlockSystemCS.Ypos <= 0 || BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos - 1] != ConstantStorageCS.BlockNone)
                    {
                        BlockSystemCS.IsStop = true;
                        LandingCounter++;
                    }
                }
            }
        }
    }

    //横方向に動く関数
    void BlockVertivalMove(int InsDirect)
    {
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            if (!BlockSystemCS.IsStop)
            {
                BlockSystemCS.PositionChanger(InsDirect);//位置変更
                StoreList(InsDirect);//配列格納
            }
        }
    }

    //ブロックを回転させる
    void BlockTurn(int InsDirect)
    {
        //右回転
        if (InsDirect == ConstantStorageCS.Right)
        {
            GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
            foreach (GameObject puyo in PuyoBlock)
            {
                BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
                //回転出来るブロック
                if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.OutSide)
                {
                    //Debug.Log("右回転");
                    //上から右へ
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        //右下へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Right;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.DownRight);
                        StoreList(ConstantStorageCS.DownRight);
                        //Debug.Log("右下に回転");

                    }
                    //右から下へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        //左下へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Under;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.DownLeft);
                        StoreList(ConstantStorageCS.DownLeft);
                        //Debug.Log("左下に回転");
                    }

                    //下から左へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        //左上へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Left;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.UpLeft);
                        StoreList(ConstantStorageCS.UpLeft);
                        //Debug.Log("左上に回転");
                    }

                    //左から上へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        //右上へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Over;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.UpRight);
                        StoreList(ConstantStorageCS.UpRight);
                        //Debug.Log("右上に回転");
                    }
                }

                //回転出来ないブロック
                else if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.Center)
                {
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Left;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Over;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Right;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Under;
                    }
                }
            }
        }

        //左回転
        if (InsDirect == ConstantStorageCS.Left)
        {
            GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
            foreach (GameObject puyo in PuyoBlock)
            {
                BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
                //動かせるブロック
                if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.OutSide)
                {
                    //Debug.Log("左回転");
                    //上から左へ
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        //左下へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Left;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.DownLeft);
                        StoreList(ConstantStorageCS.DownLeft);
                        //Debug.Log("左下に回転");
                    }

                    //左から下へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        //右下へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Under;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.DownRight);
                        StoreList(ConstantStorageCS.DownRight);
                        //Debug.Log("右下に回転");
                    }

                    //下から右へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {                        
                        //右上へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Right;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.UpRight);
                        StoreList(ConstantStorageCS.UpRight);
                        //Debug.Log("右上に回転");
                    }

                    //右から上へ
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        //左上へ回転
                        BlockSystemCS.BlockState = ConstantStorageCS.Over;
                        BlockSystemCS.PositionChanger(ConstantStorageCS.UpLeft);
                        StoreList(ConstantStorageCS.UpLeft);
                        //Debug.Log("右上に回転");
                    }
                }

                //動かせないブロック
                else if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.Center)
                {
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Right;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Over;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Left;
                    }

                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        BlockSystemCS.BlockState = ConstantStorageCS.Under;
                    }
                }
            }
        }
    }

    //配列を格納する関数
    void StoreList(int InsDirect)
    {
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");//ぷよを取得
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos] = BlockSystemCS.BlockColor;//ぷよのカラーを代入

            //右移動
            if (InsDirect == ConstantStorageCS.Right && BlockSystemCS.Xpos > 0)
            {
                BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos] = ConstantStorageCS.BlockNone;
            }

            //左移動
            else if (InsDirect == ConstantStorageCS.Left && BlockSystemCS.Xpos < 5)
            {
                BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos] = ConstantStorageCS.BlockNone;
            }

            //下移動
            else if (InsDirect == ConstantStorageCS.Down)
            {
                if (BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos + 1] != ConstantStorageCS.BlockNone &&
                    BlockSystemCS.BlockState != ConstantStorageCS.Under)
                {
                    BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos + 1] = ConstantStorageCS.BlockNone;
                }
            }

            if (BlockSystemCS.BlocKPosition == ConstantStorageCS.OutSide && !BlockSystemCS.IsStop)
            {
               //右上
               if (InsDirect == ConstantStorageCS.UpRight)
               {
                    BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos - 1] = ConstantStorageCS.BlockNone;
               }

                //左上
                else if (InsDirect == ConstantStorageCS.UpLeft)
                {
                    BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos - 1] = ConstantStorageCS.BlockNone;
                }

                //右下
                else if (InsDirect == ConstantStorageCS.DownRight)
                {
                    BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos + 1] = ConstantStorageCS.BlockNone;
                }

                //左下
                else if (InsDirect == ConstantStorageCS.DownLeft)
                {
                    BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos + 1] = ConstantStorageCS.BlockNone;
                }
            }
        }
    }

    //移動できるかを調べる
    //出来ない時点でfalseを返す
    bool CanMoveSer(int InsDirect)
    {
        //左方向
        if (InsDirect == ConstantStorageCS.Left)
        {
            GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
            foreach (GameObject puyo in PuyoBlock)
            {
                BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();

                //中心を見る
                if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.Center)
                {
                    //左に外側ブロックあり
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        //Xが２未満
                        if (BlockSystemCS.Xpos < 2)
                        {
                            return false;
                        }
                        //２よりデカいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos - 2, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }                        
                    }
                    //左に外側ブロック無し
                    else if (BlockSystemCS.BlockState != ConstantStorageCS.Right)
                    {
                        //Xが１未満
                        if (BlockSystemCS.Xpos < 1)
                        {
                            return false;
                        }
                        //１よりデカいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                }

                //外側を見る
                else if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.OutSide)
                {
                    //状態が上もしくは下で左にブロックがある。もしくはXが１未満
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over || BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        //Xが１未満
                        if (BlockSystemCS.Xpos < 1)
                        {
                            return false;
                        }
                        //１よりデカいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                }
            }
            //２個ともfalse出なかった
            Debug.Log("移動できる");
            return true;
        }

        //右方向
        else if (InsDirect == ConstantStorageCS.Right)
        {
            GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
            foreach (GameObject puyo in PuyoBlock)
            {
                BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
                //ブロック中心
                if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.Center)
                {
                    //右にブロックがある。
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        //Xが4よりでかい
                        if (BlockSystemCS.Xpos >= 4)
                        {
                            return false;
                        }
                        //４より小さいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos + 2, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                    //右にブロック無し
                    else if (BlockSystemCS.BlockState != ConstantStorageCS.Right)
                    {
                        //Xが5よりでかい
                        if (BlockSystemCS.Xpos >= 5)
                        {
                            return false;
                        }
                        //5より小さいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                }

                //ブロック外側
                else if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.Over)
                {
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over || BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        //Xが5よりでかい
                        if (BlockSystemCS.Xpos >= 5)
                        {
                            return false;
                        }
                        //5より小さいけど空じゃない
                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                }
            }
            //Debug.Log("移動できる");
            return true;
        }

        Debug.Log("えらー");
        return false;
    }

    //回転できるかどうかをしらべる。
    //出来ない時点でfalseを返す。
    bool CanTurnSer(int InsDirect)
    {
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            if (!BlockSystemCS.IsStop && BlockSystemCS.BlocKPosition == ConstantStorageCS.OutSide)
            {
                //左回転
                if (InsDirect == ConstantStorageCS.Left)
                {
                    //左下を調べる
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        //ｘが０でないかつｙも０でない
                        if (BlockSystemCS.Xpos < 1 || BlockSystemCS.Ypos < 1)
                        {
                            return false;
                        }
                        //ひだりしたのブロックが空ではない
                        else if (BlockList[BlockSystemCS.Xpos - 1,BlockSystemCS.Ypos - 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }

                    //右上を調べる
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        if (BlockSystemCS.Xpos >= 5)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos + 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }

                    //右下を調べる
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        if (BlockSystemCS.Xpos >= 5 || BlockSystemCS.Ypos < 1)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos - 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }

                    //左上を調べる
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        if (BlockSystemCS.Xpos < 1)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos + 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }

                }
                //右回転
                else if (InsDirect == ConstantStorageCS.Right)
                {
                    //右下を調べる
                    if (BlockSystemCS.BlockState == ConstantStorageCS.Over)
                    {
                        if (BlockSystemCS.Xpos >= 5 || BlockSystemCS.Ypos < 1)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos - 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                    //左上を調べる
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Under)
                    {
                        if (BlockSystemCS.Xpos < 1)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos + 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                    //右上を調べる
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Left)
                    {
                        if (BlockSystemCS.Xpos >= 5)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos + 1, BlockSystemCS.Ypos + 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                    //左下を調べる
                    else if (BlockSystemCS.BlockState == ConstantStorageCS.Right)
                    {
                        if (BlockSystemCS.Xpos < 1 || BlockSystemCS.Ypos < 1)
                        {
                            return false;
                        }

                        else if (BlockList[BlockSystemCS.Xpos - 1, BlockSystemCS.Ypos - 1] != ConstantStorageCS.BlockNone)
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    //時間を管理する関数
    void TimeManagerFun()
    {
        if (AIPlayer.IsAIPlayerActive)
        {
            ManagerTime += Time.deltaTime;

            if (SystemPhase == ConstantStorageCS.MovePhase && ManagerTime >= ConstantStorageCS.jugdeProcessTime)
            {
                IsAIJudge = true;
                ManagerTime = 0; 
            }

            else if (SystemPhase == ConstantStorageCS.EraseBlockPhase && ManagerTime >= ConstantStorageCS.jugdeProcessTime)
            {
                IsGotoNextPhase = true;
                ManagerTime = 0;
            }
            else if (SystemPhase == ConstantStorageCS.AfterErasePhase && ManagerTime >= ConstantStorageCS.jugdeProcessTime)
            {
                IsGotoNextPhase = true;
                ManagerTime = 0;
            }

        }
        else
        {
            if (IsFall)
            {
                IsFall = false;
            }

            //時間を足す
            ManagerTime += Time.deltaTime;

            //規定の時間に達したとき
            if (SystemPhase == ConstantStorageCS.MovePhase && ManagerTime >= ConstantStorageCS.FallProcessTime)
            {
                IsFall = true;
                ManagerTime = 0;

            }
            else if (SystemPhase == ConstantStorageCS.EraseBlockPhase && ManagerTime >= ConstantStorageCS.jugdeProcessTime)
            {
                IsGotoNextPhase = true;
                ManagerTime = 0;
            }
            else if (SystemPhase == ConstantStorageCS.AfterErasePhase && ManagerTime >= ConstantStorageCS.jugdeProcessTime)
            {
                IsGotoNextPhase = true;
                ManagerTime = 0;
            }
        }
    }

    //操作できるぷよが着地したときにぷよ等を管理する関数
    void BlockManagerfunc()
    {
        //ぷよが１こしか着地しなかった
        if (LandingCounter == 1)
        {
            OneSideLanding();
            Debug.Log("片方だけ落ちた");
        }

        //ぷよが２個とも着地した
        if (LandingCounter == 2)
        {
            SystemPhase = ConstantStorageCS.EraseSearthPhase;//消せる物があるか探す段階へ
            LandingCounter = 0;//ランディングカウンターの初期化;
        }

        //消去出来るぷよを探す段階
        else if (SystemPhase == ConstantStorageCS.EraseSearthPhase)
        {
            BlockEraseSearth();//消す配列の作成
            IsSearchDestory = true;
            SystemPhase = ConstantStorageCS.EraseBlockPhase;//ブロックを消す段階
        }

        //消去段階
        //ここで一旦0.5秒止める
        else if (SystemPhase == ConstantStorageCS.EraseBlockPhase && IsGotoNextPhase)
        {
            //調べれるのを終了させる
            IsSearchDestory = false;
            SystemPhase = ConstantStorageCS.AfterErasePhase;//消去後の段階へ

            IsGotoNextPhase = false;//時間を管理するフラグをオフ
        }

        //下に敷き詰める段階
        else if (SystemPhase == ConstantStorageCS.AfterErasePhase && IsGotoNextPhase)
        {
            //敷き詰める関数
            AfterInsertPuyo();

            SystemPhase = ConstantStorageCS.InstantatePhase;//次の段階へ
            IsGotoNextPhase = false;//時間を管理するフラグをオフ
        }

        //生成段階
        else if (SystemPhase == ConstantStorageCS.InstantatePhase)
        {
            //ここに敷き詰めた後にもう一回サーチする関数を居れる。
            BlockEraseSearth();

            //そのあとに消せるぷよがあるかどうかを調べる関数を作り、そこで分岐する。
            //消せるぷよがなかった
            if (IsGoToNextPhase())
            {
                //DebugBlockList();
                IsInstantiate = true;//ぷよを生成させる
                SystemPhase = ConstantStorageCS.MovePhase;//動作中の段階へ
            }
            //消せるぷよがあった
            else
            {
                //消去段階前と同じことをする。
                IsSearchDestory = true;
                SystemPhase = ConstantStorageCS.EraseBlockPhase;
            }
        }
    }

    void BlockEraseSearth()
    {
        //再起処理配列の初期化
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                RevivalBlockList[x, y] = false;
                BlockEraseList[x, y] = false;
            }
        }

        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                if ((BlockList[x,y] != ConstantStorageCS.BlockNone) && (!RevivalBlockList[x,y]))
                {
                    CopyBlockListFunc();//配列のこぴーを作成
                    RevivalBlockList[x, y] = true;//調べた事がある判定にする
                    BlockCount(x,y,0,BlockList[x,y]);//周りを調べる
                }
            }
        }
    }

    //消せるブロックがあるかどうか調べる
     bool IsGoToNextPhase()
     {
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                if (BlockEraseList[x,y])
                {
                    return false;
                }
            }
        }
        return true;
     }

    public void BlockCount(int _x,int _y,int StockNum,int InsBlockColor)
    {
        //下方向に同じ色のぷよがある場合(調べる中心の座標が１以上かつ下のぷよが同じ色かつその下のぷよを一回も調べたことが無い)
        if (_y >= 1 && BlockList[_x,_y - 1] == InsBlockColor && !RevivalBlockList[_x, _y - 1])
        {
            StockNum++;//連続する個数を足す
            RevivalBlockList[_x, _y - 1] = true;//一回調べた状態にする。
            CopyBlockList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyBlockList[_x, _y - 1] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            BlockCount(_x, _y - 1, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の上方向
        if (_y <= 7 && BlockList[_x, _y + 1] == InsBlockColor && !RevivalBlockList[_x, _y + 1])
        {
            StockNum++;//連続する個数を足す
            RevivalBlockList[_x, _y + 1] = true;//一回調べた状態にする。
            CopyBlockList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyBlockList[_x, _y + 1] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            BlockCount(_x, _y + 1, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の左方向
        if (_x >= 1 && BlockList[_x - 1, _y] == InsBlockColor && !RevivalBlockList[_x - 1, _y])
        {
            StockNum++;//連続する個数を足す
            RevivalBlockList[_x - 1, _y] = true;//一回調べた状態にする。
            CopyBlockList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyBlockList[_x - 1, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            BlockCount(_x - 1, _y, StockNum, InsBlockColor);//その下を今度は調べる
        }

        //以下同の右方向
        if (_x <= 4 && BlockList[_x + 1, _y] == InsBlockColor && !RevivalBlockList[_x + 1, _y])
        {
            StockNum++;//連続する個数を足す
            RevivalBlockList[_x + 1, _y] = true;//一回調べた状態にする。
            CopyBlockList[_x, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            CopyBlockList[_x + 1, _y] = ConstantStorageCS.TmpDeleteColor;//消すかも判定にしとく
            BlockCount(_x + 1, _y, StockNum, InsBlockColor);//その下を今度は調べる
        }
        //Debug.Log("ストックなむ  " + StockNum);
        //調べたけ上下左右にぷよがなかったが、個数が４個以上あった場合
        if (StockNum >= 3)
        {
            for (int x = 0; x < 6; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    if (CopyBlockList[x, y] == ConstantStorageCS.TmpDeleteColor)
                    {
                        BlockEraseList[x, y] = true;
                        Debug.Log("個の色を消す  " + x + "  ｘ  " + y + "   y  ");
                    }
                }
            }
        }

    }

    //ぷよのコピーを生成
    void CopyBlockListFunc()
    {
        for (int x = 0; x < 6; x++)
        {
            for (int y = 0; y < 9; y++)
            {
                CopyBlockList[x, y] = BlockList[x, y];
            }
        }
    }

    //ぷよのデータを取得して表示
    void DebugPuyoObjectsFunc()
    {
        GameObject[] Puyos = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in Puyos)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            Debug.Log("ぷよのポジション  X  " + BlockSystemCS.Xpos + "  Y  " + BlockSystemCS.Ypos +"  カラー　" + BlockSystemCS.BlockColor );
        }
    }

    //敷き詰め関数
    void AfterInsertPuyo()
    {
        //ぷよをゲットする
        GameObject[] Puyos = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in Puyos)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            //Ｙ座標が０より大きかったら（０の時は最下層）
            if (BlockSystemCS.Ypos > 0)
            {
                int DownNum = 0;//どのぐらい下がるかをカウントする奴。
                //自分のＹ座標の１個下から調べる
                for (int y = BlockSystemCS.Ypos - 1; y >= 0; y--)
                {
                    //ぷよがなかったら
                    if (BlockList[BlockSystemCS.Xpos, y] == ConstantStorageCS.BlockNone)
                    {
                        DownNum++;//カウントを足す。
                    }
                }
                //Debug.Log("DownNum  " + DownNum);
                //カウントが０より大きいなら
                if (DownNum > 0)
                {
                    //描画位置をずらす
                    puyo.gameObject.transform.position -= new Vector3(0, ConstantStorageCS.MasSize * DownNum, 0);
                    //Debug.Log("敷き詰め関数発dou");
                    //今いる位置の配列に色を代入し、元居た位置にを空にする。
                    BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos - DownNum] = BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos];
                    BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos] = ConstantStorageCS.BlockNone;
                    BlockSystemCS.Ypos -= DownNum;
                }
            }
        }
    }

    //そのぷよの１番下を調べる関数
    //引数は調べたいぷよのｙ座標、ｘ座標
    //返り値は一番下のy座標
    int MostUnderOfPuyo(int InsY,int InsX)
    {
        for (int y = InsY - 1;y >= 0; y--)
        {
            //配列にぶろっくがあったら
            if (BlockList[InsX,y] != ConstantStorageCS.BlockNone )
            {
                return y;
            }
        }

        return 1;
    }

    //ランディングカウンターが１の時（片方がついていてもうかたほうがついていないとき）
    void OneSideLanding()
    {
        GameObject[] PuyoBlock = GameObject.FindGameObjectsWithTag("Puyo");
        foreach (GameObject puyo in PuyoBlock)
        {
            BlockSystem BlockSystemCS = puyo.gameObject.GetComponent<BlockSystem>();
            //止まってなかったとき
            if (!BlockSystemCS.IsStop)
            {
                //リターンで返す
                int DownNum = MostUnderOfPuyo(BlockSystemCS.Ypos, BlockSystemCS.Xpos);
                Debug.Log("一番下" + DownNum);

                //AfterInsertPuyoと同じ動作をする。
                if (DownNum > 0)
                {
                    //描画位置をずらす
                    puyo.gameObject.transform.position -= new Vector3(0, ConstantStorageCS.MasSize * DownNum, 0);
                    //今いる位置の配列に色を代入し、元居た位置にを空にする。
                    BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos - DownNum] = BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos];
                    BlockList[BlockSystemCS.Xpos, BlockSystemCS.Ypos] = ConstantStorageCS.BlockNone;
                    BlockSystemCS.Ypos -= DownNum;

                    //ブロックを動けなくしてランディングカウンターを足す
                    BlockSystemCS.IsStop = true;
                    LandingCounter++;
                }
            }
        }
    }

    //スペースを押したら最下層まで行く関数(スペースが押されたかは動きを管理する関数で判断)
    void GotoMostUnder()
    {
        while (LandingCounter < 2)
        {
            BlockFall();
        }      
    }

    //ブロックのゲームシステム側の配列をデバックする関数
    void DebugBlockList()
    {
        for (int X = 0; X < 6; X++)
        {
            for (int Y = 0; Y < 9; Y++)
            {
                if (BlockList[X,Y] != ConstantStorageCS.BlockNone)
                {
                    Debug.Log("ブロックが入ってるところ　" + X + "  " + Y + "  "+BlockList[X,Y]);
                }
            }
        }
    }

}
